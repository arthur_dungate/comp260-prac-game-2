﻿using UnityEngine;
using System.Collections;

public class AIPaddle : MonoBehaviour {

	public Transform puck;
	private Rigidbody rigidbody;
	public float acceleration = 0.1f;
	public float movementThreshold = 1.0f;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 dir = puck.position - transform.position;

		Vector3 force = Vector3.zero;
		if (dir.y > movementThreshold) {
			force = Vector3.up;		
		} else if (dir.y < -movementThreshold) {
			force = Vector3.down;
		}
		rigidbody.AddForce(force * acceleration);

	}
}
