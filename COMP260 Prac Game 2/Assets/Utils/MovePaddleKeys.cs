﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody))]
public class MovePaddleKeys : MonoBehaviour
{

	private Rigidbody rigidbody;
	public float speed = 20f;

	void Start ()
	{
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false;
	}

	void FixedUpdate() {
		// get the input values
		Vector3 direction;
		direction.x = Input.GetAxis("Horizontal");
		direction.y = Input.GetAxis("Vertical");
		direction.z = 0;

		// scale by the maxSpeed parameter
		rigidbody.velocity = direction * speed;

	}





}
